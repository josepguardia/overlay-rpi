Freon based overlay on chromiumos for the raspberrypi board.

This is to be used for latest Freon based graphics stack in ChromiumOS. The older x11 based graphics stack has been fully deprecated in ChromiumOS R57 onwards.
